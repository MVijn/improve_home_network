## debug tools

# an Ubuntu example

### open a shell

Press CRTL-ALT-T

###  install some networking packages

Install packages:
```
sudo apt install iputils-ping inetutils-traceroute
```

First find out what your default gateway (modem's ip number) is. There are couple of way to find out:


### Ip Route

Type in a shell: 
```
ip r
```

Demo output: 

```
default via 192.168.178.1 dev enx00e04c68003f proto dhcp metric 100 
default via 192.168.178.1 dev wlp3s0 proto dhcp metric 600 
169.254.0.0/16 dev enx00e04c68003f scope link metric 1000 
192.168.178.0/24 dev enx00e04c68003f proto kernel scope link src 192.168.178.84 metric 100 
192.168.178.0/24 dev wlp3s0 proto kernel scope link src 192.168.178.70 metric 600 
```
 
The default gateway is
```
192.168.178.1
```

### Trace Route 

Test the route over internet via traceroute

```
traceroute 8.8.8.8
```

Demo output

```
traceroute to 8.8.8.8 (8.8.8.8), 64 hops max
  1   192.168.178.1  1,123ms  0,804ms  0,724ms 
  2   194.109.5.204  8,086ms  7,129ms  6,870ms 
  3   194.109.7.205  7,891ms  7,163ms  7,235ms 
  4   194.109.5.25  7,995ms  7,440ms  7,201ms 
  5   194.109.11.2  8,067ms  7,658ms  8,983ms 
  6   *  *  * 
  7   209.85.240.115  8,699ms  7,548ms  8,153ms 
  8   8.8.8.8  8,294ms  7,339ms  7,963ms 
```

The first gateway is:
```
192.168.178.1
```


## Testing the Latency (Delay)

Use ping to see the latency:

Here send 10 packets to the gateway and wait for results:

```
ping -c 10 192.168.178.1
``` 

Demo output of a pc on a cable:

```
PING 192.168.178.1 (192.168.178.1) 56(84) bytes of data.
64 bytes from 192.168.178.1: icmp_seq=1 ttl=64 time=0.761 ms
64 bytes from 192.168.178.1: icmp_seq=2 ttl=64 time=1.14 ms
64 bytes from 192.168.178.1: icmp_seq=3 ttl=64 time=1.01 ms
64 bytes from 192.168.178.1: icmp_seq=4 ttl=64 time=0.760 ms
64 bytes from 192.168.178.1: icmp_seq=5 ttl=64 time=0.775 ms
64 bytes from 192.168.178.1: icmp_seq=6 ttl=64 time=0.763 ms
64 bytes from 192.168.178.1: icmp_seq=7 ttl=64 time=0.865 ms
64 bytes from 192.168.178.1: icmp_seq=8 ttl=64 time=1.11 ms
64 bytes from 192.168.178.1: icmp_seq=9 ttl=64 time=0.807 ms
64 bytes from 192.168.178.1: icmp_seq=10 ttl=64 time=0.967 ms

--- 192.168.178.1 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9121ms
rtt min/avg/max/mdev = 0.760/0.897/1.147/0.145 ms
```

So here:
- The time is the lacenty in milliseconds, lower is better. 
- There is no packet loss

Demo on good WiFi: 
```
PING 192.168.178.1 (192.168.178.1) 56(84) bytes of data.
64 bytes from 192.168.178.1: icmp_seq=1 ttl=64 time=2.13 ms
64 bytes from 192.168.178.1: icmp_seq=2 ttl=64 time=2.71 ms
64 bytes from 192.168.178.1: icmp_seq=3 ttl=64 time=2.68 ms
64 bytes from 192.168.178.1: icmp_seq=4 ttl=64 time=2.42 ms
64 bytes from 192.168.178.1: icmp_seq=5 ttl=64 time=2.41 ms
64 bytes from 192.168.178.1: icmp_seq=6 ttl=64 time=2.99 ms
64 bytes from 192.168.178.1: icmp_seq=7 ttl=64 time=2.50 ms
64 bytes from 192.168.178.1: icmp_seq=8 ttl=64 time=2.82 ms
64 bytes from 192.168.178.1: icmp_seq=9 ttl=64 time=2.86 ms
64 bytes from 192.168.178.1: icmp_seq=10 ttl=64 time=2.41 ms

--- 192.168.178.1 ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9014ms
rtt min/avg/max/mdev = 2.136/2.597/2.999/0.257 ms
```

So here is: 
- more but acceptable latency
- no packetloss

### Demo on bad WiFi:
 
```
PING 192.168.178.1 (192.168.178.1) 56(84) bytes of data.
64 bytes from 192.168.178.1: icmp_seq=1 ttl=64 time=7.06 ms
64 bytes from 192.168.178.1: icmp_seq=2 ttl=64 time=6.80 ms
64 bytes from 192.168.178.1: icmp_seq=3 ttl=64 time=7.74 ms
64 bytes from 192.168.178.1: icmp_seq=4 ttl=64 time=4.16 ms
64 bytes from 192.168.178.1: icmp_seq=5 ttl=64 time=3.89 ms
64 bytes from 192.168.178.1: icmp_seq=6 ttl=64 time=3.50 ms
64 bytes from 192.168.178.1: icmp_seq=7 ttl=64 time=4.52 ms

--- 192.168.178.1 ping statistics ---
10 packets transmitted, 7 received, 30% packet loss, time 9064ms
rtt min/avg/max/mdev = 3.503/5.386/7.747/1.622 ms
```

So here:
- more latency
- packet loss


