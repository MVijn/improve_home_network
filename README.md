# How improve your home network for conferencing


In this place a manual to help to improve you home network,
but may apply to small business. 

## The Why

Having a slow or instable network is really annoying in realtime
communication. It is simply disruptive in the communication and focus.

### The What

An example of a home network:

```mermaid
graph TD
  subgraph " Home Network"
  Computer -- WiFi --> Modem;
  Phone -- WiFi --> Modem;
  Notebook -- WiFi --> Modem;
  Tablet -- WiFi --> Modem;
  Modem --> Internet;
  Internet --> ComputerofConferencingPartner;
  Internet --> ConferencingServer;
  end
```
- Routing, is there a path from the computer to a Server?
- Latency, is this path quick?
- BandWith, how much traffic can we send simulatinously? 

For VoiceConferencing is latency important. The latest is 
important if you have multiple users.  

Latency can be easily measured in ping and traceroute (see debugging)

## Cables, Wifi and Bluetooth

Cables are the fast and have less latency. If you have one, use it.

Wifi is shared medium, so your house mates, neighbourgs use the
same radio bands. While they are not necessarily the same wifi networks, they use the same spectrum and may cause interference and that is bad. There are two bands 2.4 (b/g) and 5 Ghz (a). Not all devices (Pc's/notebooks/Modems/Phones) have 5Ghz, most newerones do.

If don't have cable move closer to your WiFi point. Your could
test if the latency changes if you open a ping (see debugging) and walk 
around with your notebook to see if changes.

Bluetooth is also in the 2.4 Ghz. So if your WiFi is bad and your notebook is connected 2.4Ghz adding a bluetooh headset may make it worse. 

Replacing it with a wired headset may help. I have always a spare headset
around in case my bluetooth fails. So again ... cables.... 


##  Debug Tools

There separate pages for Mac, Windows and Linux . 

## Easy Solutions

Easy (but ugly) wire every thing with cables. Most modems (ADSL/Cable) heeft free interface that can connect a network cable to you computer or notebook. If you are out of interfaces buy a small switch.

```mermaid
graph TD
  subgraph "Expand switch layer"
  Modem -- networkcable --> Switch;
  Modem -- networkcable --> computer1;
  Modem -- networkcable --> computer2;
  Modem -- networkcable --> computer3;
  Switch -- networkcable --> computer4; 
  Switch -- networkcable --> computer5; 
  Switch -- networkcable --> computer6;
end
``` 




May you need to buy also a USB-ethernet adapter. If need to choose between usb and ubs-c , I recommend the usb-c.

Another option is HomePlug, here you use the powerlines in the walls to make themact as network cables. More expensive 

```mermaid
  graph TB
  subgraph "Home Plug cable example"
  Modem -- networkcable --> HomePlug1[ Home Plug 1]; 
  HomePlug1 -- 230Net -->  HomePlug2[ Home Plug 2];
  HomePlug2 -- network_cable --> Computer;
  end
```


## Phone and Tablets
 
The above does not work for phone and tablets. But there are also HomePlug Wifi extenders. 

Such may work, but does not beat the cable approach:

```mermaid
  graph TB
  subgraph "Home Plug Wifi example"
  Modem -- networkcable --> HomePlug1[ Home Plug ]; 
  HomePlug1 -- 230Net -->  HomePlug2[ Home Plug + WiFi];
  HomePlug2 -- network_cable --> Computer;
  HomePlug2 -- WiFi --> Tablet;
  HomePlug2 -- WiFi --> Phone;
  end
```
I would not advocate for WiFi extenders the reuse the WiFi bands. The likey increase latency 


## More complex Solution

I have a couple of power users and more devices than average. Also I prefer to spend many 
to quality of life, that includes good wifi.
This setup is more an example for an larger household or a small business. For sure
it is more expensive. 


```mermaid
graph TD;
  subgraph "WiFi"
  GameTablets1 --> WiFiAccesPointA;
  GameTablets2 --> WiFiAccesPointA;
  Phone_work --> WiFiAccesPointB;
  Phone_private --> WiFiAccesPointC;
  NoteBookNetflix --> WiFiAccesPointC;
  end
  subgraph "Wired Network"
  NoteBookWork --> Switch;
  NoteBookSchoolWork --> Switch;
  NoteBookPrivatePojects --> Switch;
  NoteBookNetflix --> Switch;
  WiFiAccesPointA --> Switch;
  WiFiAccesPointB --> Switch;
  WiFiAccesPointC --> Switch;
  Switch --> Modem; 
  Modem --> Internet;
  end
  subgraph "Machines on Internet"
  Internet --> ComputerofConferencingPartner;
  Internet --> ConferencingServer;
  Internet --> GameServer;
  end
```
```
 Note 0 : Here the modem has the WiFi switched off 
 Note 1 : The switch powers the access points.
 Note 2 : This requires a lot of cabling.  
```

 If you need this; it will take some time and effort to learn all this. 
